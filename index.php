<!DOCTYPE html>
<html>
	<head>
		<title>Yu-Gi-Oh!</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="js/jquery-3.3.1.min.js"></script>
		<link rel="stylesheet" href="css/styles.css">
		<noscript>
			<p>You have to enable Javascript to your browser</p>
		</noscript>
		<script>
		var greg_createCookie = function(name, value, days=1) {
			let expires;
			if (days) {
				let date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
			}
			else {
				expires = "";
			}
			document.cookie = name + "=" + value + expires + "; path=/";
		}
		
		if(document.cookie.indexOf("compare_cards") == -1){
			var compare_cards = [];
			let str_2_save = JSON.stringify(compare_cards);
			greg_createCookie('compare_cards',str_2_save);
		}else{
			var compare_cards = JSON.parse(getCookie("compare_cards"));
		}
		
		function getCookie(c_name) {
			if (document.cookie.length > 0) {
				c_start = document.cookie.indexOf(c_name + "=");
				if (c_start != -1) {
					c_start = c_start + c_name.length + 1;
					c_end = document.cookie.indexOf(";", c_start);
					if (c_end == -1) {
						c_end = document.cookie.length;
					}
					return unescape(document.cookie.substring(c_start, c_end));
				}
			}
			return "";
		}
		
		/*
			array with given Yu-Gi-Oh cards
		*/
		var cards = ['Burial from a Different Dimension',
					'Charge of the Light Brigade',
					'Infernoid Antra',
					'Infernoid Attondel',
					'Infernoid Decatron',
					'Infernoid Devyaty',
					'Infernoid Harmadik',
					'Infernoid Onuncu',
					'Infernoid Patrulea',
					'Infernoid Pirmais',
					'Infernoid Seitsemas',
					'Lyla, Lightsworn Sorceress',
					'Monster Gate',
					'One for One',
					'Raiden, Hand of the Lightsworn',
					'Reasoning',
					'Time-Space Trap Hole',
					'Torrential Tribute',
					'Upstart Goblin',
					'Void Seer'];
		
		$(document).ready(function(){
			
			/*
				listen to the btn-button clicks,
				make active the clicked button and deactivate the rest
			*/			
			$(document).on('click', '.btns' , function() {
				$('.btns').removeClass('active');
				$(this).addClass('active');
			});
			
			$(document).on('click', '.compare-me' , function() {
				$(this).toggleClass('success')
			});
			
			
			load_cards();
			$('#compare_btn').html('Compare: '+compare_cards.length+' cards');			
		});
		
		function clear_selected_btn(){
			$('.btns').removeClass('active');
		}
		
		function load_cards(){
			/*
				check if cards are already saved at localstorage
				if not: load them at localstorage and then list them
				else: just list them
			*/
			if(localStorage.getItem(cards[0]) === null){
				let itemsProcessed = 0;
				cards.forEach(function(card){
					let card_name = card.split(' ').join('%20');
					$.getJSON("http://52.57.88.137/api/card_data/"+card_name, function(result){
						$.each(result, function(index, field, array){
							if(index == 'data'){
								localStorage.setItem(card_name, JSON.stringify(field));
								itemsProcessed++;
								if(itemsProcessed === cards.length) {
									list_cards();
								}
							}
						});
					});
				});
				
			}else{
				list_cards();
			}
			
		}
		
		function list_cards(){
			let build = '';
			$.each(cards,function(index,card){
				let card_name = card.split(' ').join('%20');
				/*
					get card's details from localstorage
				*/
				let field = JSON.parse(localStorage.getItem(card_name));
				build += '<div class="col-12 card btns btn cursor-pointer" onclick="get_card_details(\''+card_name+'\');">';
				build += '<span class="name col-12">' + field.name + '</span>';
				build += '<span class="card_type '+field.card_type+'">' + field.card_type + '</span>';
				build += '</div>';
			});
			
			$("#available-cards").html(build);
		}
		
		function get_card_details(chosen_one){
			let build = '';
			if(localStorage.getItem(chosen_one) !== null){
				let field = JSON.parse(localStorage.getItem(chosen_one));
				let image = '<img src="http://52.57.88.137/api/card_image/'+chosen_one+'" />';
				
				let compare_class ='';
				let compare_cards = JSON.parse(getCookie("compare_cards"));
				
				if(compare_cards.includes(chosen_one.split('%20').join(' '))){
					 compare_class ='success';
				}
				
				build += '<div class="col-12">';
				build += '<span class="image col-2"><button type="button" class="btn col-12 compare-me cursor-pointer '+compare_class+'" onclick="add_to_compare(\''+chosen_one+'\');">Add to compare</button>' + image + '</span>';
				
				let get_fields = Object.keys(field);
				$.each(get_fields,function(index,data){
					if(field[data] === null){
						field[data] = '-';
					}
					let onclick = '';
					let extra_class = '';
					if(field[data] != '-' && (data == 'card_type' || data == 'type' || data == 'family' || data == 'atk' || data == 'def' || data == 'level' || data == 'property'))
					{
						onclick = 'onclick="list_all_similar(\''+data+'\',\''+field[data]+'\');"';
						extra_class = 'cursor-pointer';
					}
					
					if(data == 'card_type'){
						build += '<span class="card-field '+data+' '+extra_class+' col-10" '+onclick+'><label class="card-field-label">'+data+':</label><span class="'+data+' '+field[data]+'">' + field[data]  + '</span></span>';
					}else{
						build += '<span class="card-field '+data+' '+extra_class+' col-10" '+onclick+'><label class="card-field-label">'+data+':</label>' + field[data]  + '</span>';
					}
				});
				
				build += '</div>';
			}else{
				build = '<div class="col-12 error text-center">Card does not exist</span>';
			}
			$("#details").html(build);
		}
		
		function list_all_similar(type, value){
			clear_selected_btn();
			let card_type = type.split('_').join(' ');
			
			let build = '<h2 class="list-similar-title col-12 text-center">'+card_type+' : "'+value+'"</h2>';
			$.each(cards,function(index,card){
				let card_name = card.split(' ').join('%20');
				let field = JSON.parse(localStorage.getItem(card_name));
				let get_fields = Object.keys(field);
				
				if(field[type] == value)
				{
						
						let image = '<img src="http://52.57.88.137/api/card_image/'+card_name+'" />';
						build += '<div class="col-3 to-compare">';
						
						let compare_class ='';
						let compare_cards = JSON.parse(getCookie("compare_cards"));
						if(compare_cards.includes(card_name.split('%20').join(' '))){
							 compare_class ='success';
						}
						build += '<span class="image col-12"><button type="button" class="btn col-12 compare-me cursor-pointer '+compare_class+'" onclick="add_to_compare(\''+card_name+'\');">Add to compare</button>' + image + '</span>';
						$.each(get_fields,function(index,data){
							if(field[data] === null){
								field[data] = '-';
							}
							let onclick = '';
							let extra_class = '';
							if(field[data] != '-' && (data == 'card_type' || data == 'type' || data == 'family' || data == 'atk' || data == 'def' || data == 'level' || data == 'property'))
							{
								onclick = 'onclick="list_all_similar(\''+data+'\',\''+field[data]+'\');"';
								extra_class = 'cursor-pointer';
							}
							if(data == 'card_type'){
								build += '<span class="card-field '+data+' '+extra_class+' col-10" '+onclick+'><label class="card-field-label">'+data+':</label><span class="'+data+' '+field[data]+'">' + field[data]  + '</span></span>';
							}else{
								build += '<span class="card-field '+data+' '+extra_class+' col-10" '+onclick+'><label class="card-field-label">'+data+':</label>' + field[data]  + '</span>';
							}
							/*
							build += '<span class="card-field '+data+' col-12" '+onclick+'><label class="card-field-label">'+data+':</label>' + field[data]  + '</span>';
							*/
						});
						build += '</div>';
				}
					$("#details").html(build);
			});
		}
		
		
		function add_to_compare(card_to_compare){
			
			if( $.inArray(card_to_compare, compare_cards) != -1){
				//already exists: remove it
				compare_cards = jQuery.grep(compare_cards, function(value) {
					return value != card_to_compare;
				});
			} else {
				//doesn't exist: add it
				card_to_compare = card_to_compare.split(' ').join('%20');
				compare_cards.push(card_to_compare);
			}
			let str_2_save = JSON.stringify(compare_cards);
			greg_createCookie('compare_cards',str_2_save);
			$('#compare_btn').html('Compare: '+compare_cards.length+' cards');
		}
		
		
		function display_compare(){
			clear_selected_btn();
			let build = '';
			if(compare_cards.length > 0)
			{
				build = '<button class="col-12 error btn" type="button" onclick="clear_compare();">Clear all</button>';
				$.each(compare_cards,function(index,card){
					let card_name = card.split(' ').join('%20');
					let field = JSON.parse(localStorage.getItem(card_name));
					let get_fields = Object.keys(field);
					let image = '<img src="http://52.57.88.137/api/card_image/'+card_name+'" />';
					build += '<div class="col-3 to-compare">';
					build += '<span class="image col-12">' + image + '</span>';
					$.each(get_fields,function(index,data){
						if(field[data] === null){
							field[data] = '-';
						}
						let onclick = '';
						let extra_class = '';
						if(field[data] != '-' && (data == 'card_type' || data == 'type' || data == 'family' || data == 'atk' || data == 'def' || data == 'level' || data == 'property'))
						{
							onclick = 'onclick="list_all_similar(\''+data+'\',\''+field[data]+'\');"';
							extra_class = 'cursor-pointer';
						}
						if(data == 'card_type'){
							build += '<span class="card-field '+data+' '+extra_class+' col-10" '+onclick+'><label class="card-field-label">'+data+':</label><span class="'+data+' '+field[data]+'">' + field[data]  + '</span></span>';
						}else{
							build += '<span class="card-field '+data+' '+extra_class+' col-10" '+onclick+'><label class="card-field-label">'+data+':</label>' + field[data]  + '</span>';
						}
						/*
						build += '<span class="card-field '+data+' col-12" '+onclick+'><label class="card-field-label">'+data+':</label>' + field[data]  + '</span>';
						*/
					});
					build += '</div>';
				});
			}else{
				build = '<div class="col-12 error text-center">No cards to compare</div>';
			}
			$("#details").html(build);
		}
		
		function clear_compare(){
			compare_cards = [];
			let str_2_save = JSON.stringify(compare_cards);
			greg_createCookie('compare_cards',str_2_save);
			$('#compare_btn').html('Compare: '+compare_cards.length+' cards');
			display_compare();
		}
		</script>
	</head>
	<body>
		<div id="container" class="col-12">
			<div id="compare_btn" class="success col-12 btn text-center cursor-pointer" onclick="display_compare();"></div>
			<div id="available-cards" class="col-4"></div>
			<div id="details" class="col-8"></div>
		</div>
	</body>
</html>