This is a test project.

Functions:
- Loads and displays 20 specific Yu-Gi-Oh cards with their name and their type
- Saves and loads the card deck with all of the details at localstorage (only for the first time)
- On card selection it loads card's details (from localstorage)
- When we select-click on a card's field (except name, text and image field) a function is called and displays all cards with this chosen field's value
- There is a button that adds the selected card in a compare list (stored in cookie), so as to compare the cards you like. In the top of the page, there is a green button that displays the amount of cards to be compared and when you click it, all cards in compare list are being displayed